# Dirk Woodchip

> A story-driven role-playing game about a fox and a lost child.

A child woodcutter wakes up in the forest with no memories of his past and meets an unlikely companion. Embark on a quest to meet new friends, find your parents, rescue a princess and become a hero.

More or less.

---

See [[Unity]] for the implementation details.